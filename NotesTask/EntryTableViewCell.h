//
//  EntryTableViewCell.h
//  NotesTask
//
//  Created by А on 29/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EntryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *modDateLable;
@property (weak, nonatomic) IBOutlet UILabel *bodyLable;

@end

NS_ASSUME_NONNULL_END
