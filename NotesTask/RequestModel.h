//
//  RequestModel.h
//  NotesTask
//
//  Created by А on 28/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RequestModel : NSObject

-(NSDictionary*)LoadEntrie;
-(NSDictionary*)SendEntry:(NSString*)Entry;

@end

NS_ASSUME_NONNULL_END
