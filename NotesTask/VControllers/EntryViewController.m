//
//  EntryViewController.m
//  NotesTask
//
//  Created by А on 29/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

#import "EntryViewController.h"

@interface EntryViewController ()

@property (weak, nonatomic) IBOutlet UILabel *modDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;

@end

@implementation EntryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_entry) {
        NSDate* date = [NSDate dateWithTimeIntervalSince1970:[_entry[@"da"] integerValue]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.yyyy HH:mm"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
        NSString* stringFromDate = [formatter stringFromDate:date];
        
        [_dateLabel setText:[NSLocalizedString(@"DA_LABEL_PREFIX", @"") stringByAppendingString:stringFromDate]];
        
        date = [NSDate dateWithTimeIntervalSince1970:[_entry[@"dm"] integerValue]];
        stringFromDate = [formatter stringFromDate:date];
        [_modDateLabel setText:[NSLocalizedString(@"DM_LABEL_PREFIX", @"") stringByAppendingString:stringFromDate]];
        [_bodyLabel setText:_entry[@"body"]];
    }
}

@end
