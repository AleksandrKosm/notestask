//
//  EntryViewController.h
//  NotesTask
//
//  Created by А on 29/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EntryViewController : UIViewController

@property(weak) NSDictionary* entry;

@end

NS_ASSUME_NONNULL_END
