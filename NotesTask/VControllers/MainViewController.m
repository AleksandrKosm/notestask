//
//  MainViewController.m
//  NotesTask
//
//  Created by А on 28/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

#import "MainViewController.h"
#import "RequestModel.h"
#import "NewEntryViewController.h"
#import "EntryViewController.h"
#import "../EntryTableViewCell.h"

@interface MainViewController ()

@property(strong, atomic) RequestModel* rModel;
@property(strong, atomic) NSArray* entries;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _rModel = [[RequestModel alloc] init];
}

-(void)viewWillAppear:(BOOL)animated {
    [self updateEntries];
}

-(void)updateEntries {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary* response = [self.rModel LoadEntrie];
        if ([response[@"status"] intValue] == 1) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                self.entries = [response[@"data"] firstObject];
                NSLog(@"\n\n entries:\n%@", self.entries);
                [self.tableView reloadData];
            });
            return;
        }
        NSLog(@"\nError:\n%@", response[@"error"]);
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:
                                     NSLocalizedString(@"CONNECTION_ERROR_TITLE", @"")
                                     message:NSLocalizedString(response[@"error"], @"")
                                     preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:
                                    NSLocalizedString(@"CONNECTION_ERROR_BUTTON", @"")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [self updateEntries];
                                    }];
        
            [alert addAction:yesButton];
        
            [self presentViewController:alert animated:YES completion:nil];
        });
    });
}

- (IBAction)newEntryButtonPressed:(UIBarButtonItem *)sender {
    NewEntryViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewEntryVC"];
    viewController.requestModel = _rModel;
    if (viewController) {
        [self.navigationController pushViewController:viewController animated:YES];
    } else {
        NSLog(@"Can't load view controller.");
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _entries ? _entries.count : 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EntryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"entryCell" forIndexPath:indexPath];
    
    if (_entries[indexPath.row]) {
        NSDate* date = [NSDate dateWithTimeIntervalSince1970:[_entries[indexPath.row][@"da"] integerValue]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.yyyy"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
        NSString* stringFromDate = [formatter stringFromDate:date];
        
        [cell.dateLabel setText:stringFromDate];
        if (![_entries[indexPath.row][@"dm"] isEqualToString:_entries[indexPath.row][@"da"]]) {
            date = [NSDate dateWithTimeIntervalSince1970:[_entries[indexPath.row][@"dm"] integerValue]];
            stringFromDate = [formatter stringFromDate:date];
            [cell.modDateLable setText:stringFromDate];
        }
        NSString* body = _entries[indexPath.row][@"body"];
        
        const NSUInteger MAX_ENTRY_LENGTH = 200;
        
        if (body.length > MAX_ENTRY_LENGTH) {
            body = [[body substringToIndex:MAX_ENTRY_LENGTH]
                    stringByAppendingString:@"..."];
        }
        
        [cell.bodyLable setText:body];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    EntryViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EntryVC"];
    viewController.entry = _entries[indexPath.row];
    if (viewController) {
        [self.navigationController pushViewController:viewController animated:YES];
    } else {
        NSLog(@"Can't load view controller.");
    }
}

@end
