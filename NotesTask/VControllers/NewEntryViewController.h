//
//  NewEntryViewController.h
//  NotesTask
//
//  Created by А on 28/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class RequestModel;

@interface NewEntryViewController : UIViewController

@property(weak, atomic) RequestModel* requestModel;

@end

NS_ASSUME_NONNULL_END
