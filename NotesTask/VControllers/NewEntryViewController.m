//
//  NewEntryViewController.m
//  NotesTask
//
//  Created by А on 28/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

#import "NewEntryViewController.h"
#import "RequestModel.h"

@interface NewEntryViewController ()
@property (weak, nonatomic) IBOutlet UITextView *EntryTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldBottomOffset;

@end

@implementation NewEntryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)paramAnimated{
    [super viewWillAppear:paramAnimated];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification object:nil];
    
    
    [_EntryTextField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIKeyboardWillHideNotification object:nil];
}

- (IBAction)sendButtonPressed:(id)sender {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary* response = [self.requestModel SendEntry:self.EntryTextField.text];
        if ([response[@"status"] intValue] == 1) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
            return;
        }
        NSLog(@"\nError:\n%@", response[@"error"]);
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:
                                         NSLocalizedString(@"CONNECTION_ERROR_TITLE", @"")
                                         message:NSLocalizedString(response[@"error"], @"")
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:
                                        NSLocalizedString(@"CANCEL", @"")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                        }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
    });
}

#pragma mark Keyboard

-(void)keyboardWillShow:(NSNotification *)paramNotification {
    NSValue *keyboardRectAsObject =
    [[paramNotification userInfo]
     objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    _textFieldBottomOffset.constant = keyboardRect.size.height;
}

-(void)keyboardWillHide:(NSNotification *)paramNotification {
    _textFieldBottomOffset.constant = 0;
}

@end
