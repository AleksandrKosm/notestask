//
//  RequestModel.m
//  NotesTask
//
//  Created by А on 28/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

#import "RequestModel.h"

@interface RequestModel ()

@property(strong) NSString* sessionID;

@end

@implementation RequestModel

static NSString* const TOKEN = @"4WTXswC-b6-J7FUpKd";
static NSString* const BASE_URL = @"https://bnet.i-partner.ru/testAPI/";

// Этот метод формерует и производит запрос.
// При ошибк он не обрабатывает ее, а возвращает в словре данные из NSError.
-(NSDictionary*)createRequestToMethode:(NSString*)methode WithBody:(NSString*)requestBody {

    NSURL* requestURL = [NSURL URLWithString:BASE_URL];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:requestURL];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:TOKEN forHTTPHeaderField:@"token"];
    
    NSString* body = [@"a=" stringByAppendingString:methode];
    if (requestBody) {
        body = [NSString stringWithFormat:@"%@&%@", body, requestBody];
    }
    NSData* requestData = [body dataUsingEncoding:NSUTF8StringEncoding];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)requestData.length] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    dispatch_semaphore_t processingSemaphore = dispatch_semaphore_create(0);
    
    NSDictionary* __block responseDict;
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request completionHandler:
      ^(NSData* data, NSURLResponse* response, NSError* error) {
          if (!error) {
              responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
          }
          if (error) {
              responseDict = @{ @"status": @0,
                                @"error": [error localizedDescription]
                                };
          }
          
          dispatch_semaphore_signal(processingSemaphore);
      }] resume];
    
    dispatch_semaphore_wait(processingSemaphore, DISPATCH_TIME_FOREVER);
    
    return responseDict;
}

-(instancetype)init {
    if ((self = [super init])) {
        [self createSession];
    }
    return self;
}

// Тут производится поытка взять сессию из памяти и если ее нет то она запрашивается.
// по заданю не очевидно, так как сессия обычно является разовым подключением, а в задании
// "генерироваться идентификатор сессии при первом старте приложения". 
-(void)createSession {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* savedSessionID = [defaults objectForKey:@"SessionID"];
    if (savedSessionID) {
        _sessionID = savedSessionID;
        return;
    }
    
    NSDictionary* response = [self createRequestToMethode:@"new_session" WithBody:nil];
    if (response[@"status"]) {
        if ([response[@"status"] intValue] == 1) {
            NSDictionary* data = response[@"data"];
            if (data) {
                _sessionID = data[@"session"];
                [defaults setObject:_sessionID forKey:@"SessionID"];
                return;
            }
        }
    }
}

-(NSDictionary*)LoadEntrie {
    if (!_sessionID) {
        [self createSession];
        if (!_sessionID) {
            NSLog(@"Can't make session\n");
            return nil;
        }
    }
    NSDictionary* response =
    [self createRequestToMethode:@"get_entries"
                        WithBody:[@"session=" stringByAppendingString:_sessionID]];

    return response;
}

-(NSDictionary*)SendEntry:(NSString*)Entry {
    if (!_sessionID) {
        [self createSession];
        if (!_sessionID) {
            NSLog(@"Can't make session\n");
            return nil;
        }
    }
    NSDictionary* response =
    [self createRequestToMethode:@"add_entry"
                        WithBody:[NSString stringWithFormat:@"session=%@&body=%@",
                                  _sessionID, Entry]];

    return response;
}

@end
